<?php
//index untuk mengumpulkan semua class main class
require_once("animal.php");
require_once("Ape.php");
require_once("Frog.php");

$sheep = new Animal("shaun");

echo "NAME : ".$sheep->name."<br>"; // "shaun"
echo "LEGS : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

$kodok = new Frog("Buduk");

echo "NAME : ".$kodok->name."<br>"; // "buduk"
echo "LEGS : ".$kodok->legs."<br>"; // 4
echo "Cold Blooded : ".$kodok->cold_blooded."<br>"; // "no"
echo $kodok->jump();

$sungokong = new Ape("Kera Sakti");

echo "NAME : ".$sungokong->name."<br>"; // "Kera Sakti"
echo "LEGS : ".$sungokong->legs."<br>"; // 2
echo "Cold Blooded : ".$sungokong->cold_blooded."<br>"; // "no"
echo $sungokong->yell(); 

?>